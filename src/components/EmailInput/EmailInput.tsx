import React, { Component } from "react";
import PropTypes from "prop-types";
import { Input } from "./components";
import _isEqual from "lodash/isEqual";

export interface EmailInputProps {
  charCodes: number[];
  placeholder: string;
  label: string;
  name: string;
  options: string[];
  testRegEx: RegExp;
  onValuesChange?: (values: string[]) => void;
}

interface EmailInputState {
  values: string[];
  value: string;
}

class EmailInput extends Component<EmailInputProps, EmailInputState> {
  static propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    /** ASCII charcode for the keys which should
     * trigger an item to be added to the collection */
    charCodes: PropTypes.arrayOf(PropTypes.number),
    options: PropTypes.arrayOf(PropTypes.string),
    testRegEx: PropTypes.instanceOf(RegExp),
    /* Method to expose values */
    onValuesChange: PropTypes.func,
  };

  static defaultProps = {
    placeholder: "",
    charCodes: [13], // 13: Enter
    label: "",
    testRegEx: /^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
  };

  state: EmailInputState = {
    values: [],
    value: "",
  };

  componentDidUpdate(prevProps: EmailInputProps, prevState: EmailInputState) {
    if (
      this.props.onValuesChange &&
      !_isEqual(prevState.values, this.state.values)
    )
      this.props.onValuesChange(this.state.values);
  }

  handleKeypress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const { charCodes } = this.props;
    if (charCodes.includes(e.charCode)) {
      e.preventDefault();
      this.handleItemAdd(this.state.value);
    }
  };

  handleValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ value: e.target.value });
  };

  selectMenuListItem = (item: string): void => {
    this.setState({ value: item });
  };

  handleItemAdd = (value: string) => {
    if (this.state.values.includes(value) || !value) {
      this.setState({ value: "" });
    } else {
      this.setState((prevState) => ({
        values: [...prevState.values, value],
        value: "",
      }));
    }
  };

  handleItemRemove = (value: string) => {
    const currentValues = this.state.values;
    const newValues = currentValues.filter((v: string) => v !== value);
    this.setState({ values: newValues });
  };

  render() {
    const { placeholder, label, name, options, testRegEx } = this.props;

    return (
      <Input
        values={this.state.values}
        handleItemRemove={this.handleItemRemove}
        label={label}
        placeholder={placeholder}
        value={this.state.value}
        handleKeypress={this.handleKeypress}
        handleValueChange={this.handleValueChange}
        handleItemAdd={this.handleItemAdd}
        name={name}
        options={options}
        testRegEx={testRegEx}
      />
    );
  }
}

export { EmailInput };
