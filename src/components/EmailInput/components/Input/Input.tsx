import React from "react";
import { TagInputItem } from "../";
import { PopMenu } from "../../..";
import useStyles from "./styles";

interface InputProps {
  values: string[];
  handleItemRemove: (v: string) => void;
  label: string;
  placeholder: string;
  value: string;
  handleKeypress: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  handleValueChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleItemAdd: (item: string) => void;
  name: string;
  options: string[];
  testRegEx: RegExp;
}

export const Input: React.FC<InputProps> = (props: InputProps) => {
  const {
    values,
    handleItemAdd,
    options,
    name,
    handleItemRemove,
    placeholder,
    value,
    handleKeypress,
    handleValueChange,
    testRegEx,
  } = props;
  const inputRef = React.useRef<any>(null);
  const containerRef = React.useRef<any>(null);
  const inputRect = inputRef.current?.getBoundingClientRect();
  const containerRect = containerRef.current?.getBoundingClientRect();
  const classes = useStyles({ containerWidth: containerRect?.width });

  return (
    <>
      <div
        ref={containerRef}
        className={classes.contaner}
        onClick={() => inputRect?.current?.focus()}
      >
        {values.length > 0 && (
          <div className={classes.tagContainer}>
            {values.map((v: string) => (
              <TagInputItem
                value={v}
                key={v}
                handleItemRemove={handleItemRemove}
                testRegEx={testRegEx}
              />
            ))}
          </div>
        )}
        <input
          ref={inputRef}
          name={name}
          placeholder={placeholder}
          value={value}
          type="text"
          onKeyPress={handleKeypress}
          onChange={handleValueChange}
          className={classes.input}
          autoFocus
        />
      </div>
      <PopMenu
        values={values}
        position={inputRect}
        searchValue={value}
        onSelectItem={handleItemAdd}
        items={options}
      />
    </>
  );
};
