import { createUseStyles } from "react-jss";

export default createUseStyles({
  contaner: {
    display: "flex",
    borderRadius: 8,
    backgroundColor: "white",
    boxShadow: "0px 3px 8px -7px grey",
    minHeight: 48,
    flexDirection: ({ containerWidth }) =>
      containerWidth && containerWidth < 600 ? "column" : "row",
  },
  tagContainer: {
    display: "flex",
    alignItems: "center",
    padding: "4px 0px 4px 8px",
    flexWrap: "wrap",
    justifyContent: "flex-start",
  },
  input: {
    minWidth: 220,
    border: "none",
    fontSize: 16,
    fontWeight: 400,
    lineHeight: "normal",
    backgroundColor: "transparent",
    outline: "none",
    padding: 8,
  },
});
