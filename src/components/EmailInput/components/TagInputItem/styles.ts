import { createUseStyles } from "react-jss";

export default createUseStyles({
  tag: ({ isValid }) => ({
    display: "flex",
    alignItems: "center",
    fontSize: 12,
    fontWeight: 700,
    borderRadius: 4,
    padding: 4,
    margin: "0px 1px",
    background: isValid ? "white" : "#f3b7bd",
    "&:hover": {
      background: isValid ? "#ebebeb" : "#f3b7bd",
    },
    "& > p": {
      margin: 0,
    },
  }),
  closeBtn: {
    visibility: ({ hover, isValid }) =>
      !isValid ? "visible" : hover ? "visible" : "hidden",
    marginLeft: 2,
    cursor: "pointer",
  },
  exclamationIcon: {
    background: "#f1515f",
    width: 12,
    height: 12,
    borderRadius: "100%",
    display: "flex",
    justifyContent: "center",
    color: "white",
    marginLeft: 2,
    "& > span": {
      fontSize: 10,
    },
    cursor: "pointer",
  },
});
