import React from "react";
import PropTypes from "prop-types";
import useStyles from "./styles";

const propTypes = {
  value: PropTypes.string.isRequired,
  handleItemRemove: PropTypes.func.isRequired,
  testRegEx: PropTypes.instanceOf(RegExp).isRequired,
};

interface TagInputItemProps {
  value: string;
  handleItemRemove: (v: string) => void;
  testRegEx: RegExp;
}

export const TagInputItem: React.FC<TagInputItemProps> = (
  props: TagInputItemProps
) => {
  const { value, handleItemRemove, testRegEx } = props;
  const [hover, setHover] = React.useState(false);
  const isValid = testRegEx.test(value);
  const classes = useStyles({
    hover,
    isValid,
  });
  return (
    <span
      className={classes.tag}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <p>{value}</p>

      {isValid ? (
        <span
          className={classes.closeBtn}
          role="button"
          onClick={() => handleItemRemove(value)}
        >
          <span>&times;</span>
        </span>
      ) : (
        <div
          onClick={() => handleItemRemove(value)}
          className={classes.exclamationIcon}
        >
          <span>&#33;</span>
        </div>
      )}
    </span>
  );
};

TagInputItem.propTypes = propTypes;
