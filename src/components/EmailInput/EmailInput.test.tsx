import React from "react";
import { shallow } from "enzyme";
import { EmailInput, EmailInputProps } from "..";

describe("<EmailInput>", () => {
  let props: EmailInputProps;

  beforeEach(() => {
    props = {
      charCodes: [13],
      placeholder: "PLACEHOLDER",
      label: "LABEL",
      name: "NAME",
      options: [],
      testRegEx: /^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
    };
  });

  it("Should match snapshot", () => {
    const render = shallow(<EmailInput {...props} />);
    expect(render).toMatchSnapshot();
  });
});
