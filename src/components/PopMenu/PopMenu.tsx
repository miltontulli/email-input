import React, { useState } from "react";
// import onClickOutside from "react-onclickoutside";
import useStyles from "./styles";

type clientRect = {
  x: number;
  y: number;
  width: number;
  height: number;
  top: number;
  bottom: number;
  right: number;
  left: number;
};

export interface PopMenuProps {
  items: string[];
  searchValue: string;
  onSelectItem: (item: string) => void;
  values: string[];
  position?: clientRect;
}

export const PopMenu: React.FC<PopMenuProps> = ({
  searchValue,
  items,
  values,
  onSelectItem,
  position: parentPosition,
}: PopMenuProps) => {
  const [open, setOpen] = useState<boolean>(false);
  const ref = React.useRef<any>(null);

  const getFilteredItems = React.useCallback((): string[] => {
    let result: string[] = [];
    if (searchValue && searchValue.length > 2) {
      result = items
        .filter((item) => !values.includes(item))
        .filter((item) => {
          const search: number = item.search(searchValue);
          if (search >= 0) return true;
          return false;
        });
    }
    if (result.length > 0) {
      setOpen(true);
    } else {
      setOpen(false);
    }
    return result;
  }, [items, searchValue, values]);

  const renderItems = React.useMemo(() => getFilteredItems(), [
    getFilteredItems,
  ]);

  const classes = useStyles({ position: parentPosition });

  return (
    <div className={classes.popMenuRoot} ref={ref}>
      {open && (
        <ul className={classes.list}>
          {renderItems.length > 0 &&
            renderItems.map((item) => (
              <li
                className={classes.listItem}
                key={item}
                onClick={() => onSelectItem(item)}
              >
                {item}
              </li>
            ))}
        </ul>
      )}
    </div>
  );
};
