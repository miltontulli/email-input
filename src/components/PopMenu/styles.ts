import { createUseStyles } from "react-jss";

export default createUseStyles({
  popMenuRoot: ({ position }) => {
    const baseStyle = {
      maxHeight: 200,
      overflowY: "scroll",
      width: "fit-content",
      boxShadow: "0px 0px 11px -3px grey",
      borderRadius: 4,
    };

    return position
      ? {
          position: "absolute",
          top: `${position.y + position.height - 10}px`,
          left: `${position.x + 10}px`,
          ...baseStyle,
        }
      : baseStyle;
  },
  list: {
    listStyle: "none",
    padding: 0,
    margin: 0,
    background: "white",
  },
  listItem: {
    padding: 10,
    fontSize: 14,
    "&:hover": {
      cursor: "pointer",
      background: "#ebebeb",
      boxShadow: "1px 1px 14px -9px grey",
    },
  },
});
