import React, { Component } from "react";
import { EmailInput } from "./components";
import emails from "./example/emails";

const getAsyncEmails = (): Promise<string[]> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(emails);
    }, 1500);
  });

export default class Example extends Component<
  any,
  { values: string[]; options: string[] }
> {
  state = {
    values: [],
    options: [],
  };

  componentDidMount() {
    this.simulateFetch();
  }

  simulateFetch = async () => {
    try {
      const emailOptions = await getAsyncEmails();
      this.setState({ options: emailOptions });
    } catch (e) {
      console.error("Some error msg");
    }
  };

  render() {
    return (
      <>
        <h1>Email Input Component Example:</h1>
        <EmailInput
          name="item-input"
          charCodes={[13]}
          placeholder="Enter recipients..."
          options={this.state.options}
          onValuesChange={(values: string[]) => {
            // do something with values;
            console.log(values);
            this.setState({ values });
          }}
        />
        <p>Values:</p>
        <pre>{JSON.stringify(this.state.values)}</pre>
      </>
    );
  }
}
