# Email input component demo

### You can see a live demo here -->> https://email-input.web.app/

Things to improve:

- Add more tests
- Add storybook to show how to use
- Create bundle to export as pkg component
- publish as npm package
- Add a lint check on pipeline before deploy
- Add a loading spinner if options are loading

### How to use

```javascript
<EmailInput
  /* name for the input component */
  name="item-input"
  /* ASCII charcode for the keys which should trigger an item to be added to the collection */
  charCodes={[13]}
  placeholder="Enter recipients..."
  /* Options to show on popUp Menu as an array of stings. Similar to select component */
  options={this.state.options}
  /* A handler to get the values selected */
  onValuesChange={(values) => {
    // values is an array of strings
  }}
/>
```

## Setup 🔧

```
git clone git@gitlab.com:miltontulli/email-input.git
cd email-input
yarn install
```

## Available Scripts

In the project directory, you can run:

```
yarn start
yarn jest
yarn build
```
